```mysql
select sql_calc_found_rows id,
                           activityName,
                           activityCityLimit,
                           priority,
                           status,
                           orderLevel,
                           activityStartTime,
                           activityEndTime
from organizationactivity
WHERE activityName like CONCAT('%','test', '%')
order by createTime desc
limit 0, 20;
select found_rows() as total;
```

相应的mybatis配置：

```xml
    <sql id="base_query">
      id, activityName, activityCityLimit, priority, status, orderLevel, activityStartTime, activityEndTime
    </sql>
    <sql id="params_where">
        <where>
            <if test="activityIds != null">
                and id in
                <foreach collection="activityIds" index="index" item="id" open="(" separator="," close=")">
                    #{id}
                </foreach>
                or activityCityLimit = 0
            </if>
            <if test="record.activityName != null">
                and activityName like CONCAT('%', #{record.activityName, jdbcType=VARCHAR},'%')
            </if>
            <if test="record.activityCityLimit != null">
                and activityCityLimit = #{record.activityCityLimit}
            </if>
        </where>
            <if test="record.status != null ">
                having (
                <choose>
                    <when test="record.status != null and record.status == 0 ">
                     status = #{record.status} or (status = 1 and activityEndTime &lt; now())
                    </when>
                    <when test="record.status != null and record.status == 1 ">
                      status = #{record.status} and activityEndTime >= now() and activityStartTime &lt;=now()
                    </when>
                    <when test="record.status != null and record.status == 4 ">
                        status = 1 and activityStartTime >= now()
                    </when>
                    <when test="record.status != null ">
                        status = #{record.status}
                    </when>
                </choose>
                )
            </if>

    </sql>

    <resultMap id="count" type="java.lang.Integer">
        <result column="total"/>
    </resultMap>

    <select id="queryByConditionAndPage" resultMap="BaseResultMap,count">
        select sql_calc_found_rows
        <include refid="base_query"/>
        from organizationactivity
        <include refid="params_where"/>
        order by createTime desc
        limit #{pageNo}, #{pageSize};
        select found_rows() as total;
    </select>
```



Dao 层要这样写：（它返回两个数值）

```java
    List<?> queryByConditionAndPage(@Param("record") OrganizationActivity activity, @Param("activityIds") List<Integer> activityIds, @Param("pageSize") int pageSize, @Param("pageNo") int pageNo);
```

impl 

```java
List<?> list = organizationActivityMapper.queryByConditionAndPage(oa, activityIds, pageSize, pageNo);

List<OrganizationActivity> activityList = (List<OrganizationActivity>) list.get(0);

Integer total = ((List<Integer>) list.get(1)).get(0);
```

